import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map} from 'rxjs/operators';

@Injectable()
export class SolicitudService {

  URL_SERVICIOS = 'http://localhost:4500';

  constructor(
    public http: HttpClient
  ) {

  }
  getHistorial() {

    let url = this.URL_SERVICIOS + '/api/historial/get'
    return this.http.get( url )
      .pipe(map(  (resp: any) => {
        return resp.results;
      }));

  }

}
