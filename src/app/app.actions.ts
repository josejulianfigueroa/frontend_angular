import {createAction, props} from '@ngrx/store';
import {Historial} from "./historial.model";

export const cargar = createAction(
  '[Historial] Cargar',
  props<{first_name: string, last_name: string }>());

export const obtener = createAction(
  '[Historial] Obtener',
  props<{historial: Historial[] }>());

export const vaciar = createAction(
  '[Historial] Vaciar');
