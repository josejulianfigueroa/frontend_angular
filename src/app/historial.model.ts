

export class Historial {

  public id: number;
  public first_name: string;
  public last_name:string;
  public email: string;
  public gender: string;
  public ip_address: string;

  constructor( first_name: string, last_name: string ) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = "prueba@gmail.com";
    this.ip_address = '192.168.1.1'
    this.gender = "Agender";
    this.id = Math.random();
  }

}


