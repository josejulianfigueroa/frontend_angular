import { Component, OnInit } from '@angular/core';
import {SolicitudService} from "./solicitud.service";

import { Store } from '@ngrx/store';
import * as actions from './app.actions';
import {Historial} from "./historial.model";
import {AppState} from "./app.reducer";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'appRedux';
  historial: Historial[] = [];

  constructor(
    private store: Store<AppState>,
    public solicitudService: SolicitudService
  ) {

  }

  ngOnInit() {
    this.store .select('historial')
      .subscribe( historial => {
        console.log(historial);
         this.historial = historial
      });
  }

  cargar(){

    this.store.dispatch( actions.cargar({first_name: "Juan", last_name: "Perez"})  );

  }

  vaciar() {
    this.store.dispatch(actions.vaciar());

  }

    getHistorial() {
    this.solicitudService.getHistorial()
      .subscribe( resp => {
        this.store.dispatch( actions.obtener({historial: resp}));
        },
        err => {
          console.debug(JSON.stringify(err));
        });
  }

}
