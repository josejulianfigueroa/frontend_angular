import { Action, createReducer, on} from '@ngrx/store';
import {cargar, obtener, vaciar} from './app.actions';
import {Historial} from "./historial.model";

export const initialState: Historial[] = [];

export interface AppState {
  historial: Historial[],
}

const _appReducer = createReducer(initialState,
  on(cargar, (state, {first_name, last_name}) => [...state, new Historial(first_name,last_name)]),
  on(vaciar, state => initialState),
  on(obtener,(state, {historial}) => historial)
);

export function appReducer(state: Historial[] | undefined, action: Action) {
  return _appReducer(state, action);
}
